all:SocialMain

CC = g++

CFLAGS = -Wall -g -std=c++11

SocialMain: SocialMain.o SocialData.o
	$(CC) $(CFLAGS) -o SocialData SocialData.o SocialMain.o

SocialMain.o: SocialMain.cpp
	$(CC) $(CFLAGS) -c SocialMain.cpp

SocialData.o: SocialData.cpp
	$(CC) $(CFLAGS) -c SocialData.cpp
