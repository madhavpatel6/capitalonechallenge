#include <iostream>
#include <fstream>
#include <sstream>
#include "SocialData.h"

SocialData::SocialData() {
	prediction.count = 0;
	prediction.hour = 0;
	prediction.minute = 0;
	time_min.count = 0;
	time_min.hour = 0;
	time_min.minute = 0;
	time_max.count = 0;
	time_max.hour = 0;
	time_max.minute = 0;
	setting_inst.timeframe = false;
	setting_inst.outputfile = false;
	setting_inst.inputfile = false;
	number_of_nominees = 0;
}

SocialData::~SocialData() {
	prediction.count = 0;
	prediction.hour = 0;
	prediction.minute = 0;
	system("rm -f _*.txt");
}


/*
 *	ParseCMDArgs Function 
 *	Passes argc and argv from main file as inputs and no outputs
 *	The function reads the users entered command line arguments and utilizes this information as input for other parts of the program (also throws error if certain items are not provided)
 */
void SocialData::ParseCMDArgs(int argc, char* argv[]) {
	if (argc < 2) {
		PrintUsage();
		exit(0);
	}
	std::ifstream ifs;
	int index = 1;
	char * arg;
	while(index < argc) {
		arg = argv[index];
		if ( strcmp(arg, "-o") == 0 ) {
			index++;
			ofs.open(argv[index]);
			if(!ofs.is_open())
			{
				std::cerr << "ERROR: Unable to open " << argv[index] << " for writing" << std::endl;
				this->PrintUsage();
				exit (EXIT_FAILURE);
			}
			setting_inst.outputfile = true;
		}
		else if ( strcmp(arg, "-t") == 0) {
			if(setting_inst.timeframe == true) {
				std::cerr << "ERROR: Multiple timeframes provided" << std::endl;
				this->PrintUsage();
				exit (EXIT_FAILURE);
			}
			index++;
			setting_inst.timeframe = true;
			std::stringstream ss(argv[index]);
			std::string max_buf, min_buf;
			getline(ss,min_buf, '-');
			std::stringstream ss_min(min_buf.c_str());
			std::getline(ss_min,buf,':');
			time_min.hour = std::atoi(buf.c_str());
			std::getline(ss_min,buf);
			time_min.minute = std::atoi(buf.c_str());
			getline(ss,max_buf);
			std::stringstream ss_max(max_buf.c_str());
			std::getline(ss_max,buf,':');
			time_max.hour = std::atoi(buf.c_str());
			std::getline(ss_max,buf);
			time_max.minute = std::atoi(buf.c_str());
			if(time_max.hour > 24 || time_max.minute > 60 || time_min.hour > 24 || time_min.minute > 60)
			{
				std::cerr << "ERROR: Time frame provided is not valid" << std::endl;
				this->PrintUsage();
				exit (EXIT_FAILURE);		
			}
			std::string hour_timeframe, min_minute_timeframe, max_minute_timeframe, minute_range_timeframe;
			if(time_min.hour == time_max.hour) {
				if(time_min.minute/10 == time_max.minute/10) {
					min_minute_timeframe = std::to_string(time_min.hour) + ":" + std::to_string((time_min.minute/10)) + "[" + std::to_string(time_min.minute%10) + "-" + std::to_string(time_max.minute%10) +"]:[0-5][0-9]";				
				}
				else if((time_min.minute/10) + 1 != (time_max.minute/10)) {
					hour_timeframe = std::to_string(time_min.hour) + ":[" + std::to_string((time_min.minute/10+1)) + "-" + std::to_string((time_max.minute/10-1)) + "][0-9]:[0-5][0-9]" + "|";
					min_minute_timeframe = std::to_string(time_min.hour) + ":" + std::to_string((time_min.minute/10)) + "[" + std::to_string(time_min.minute%10) + "-9]:[0-5][0-9]" + "|";
					max_minute_timeframe = std::to_string(time_max.hour) + ":" + std::to_string((time_max.minute/10)) + "[0-" + std::to_string(time_max.minute%10) + "]:[0-5][0-9]";
				}
				else if ((time_min.minute/10 + 1 == time_max.minute/10)) {
					min_minute_timeframe = std::to_string(time_min.hour) + ":" + std::to_string((time_min.minute/10)) + "[" + std::to_string(time_min.minute%10) + "-9]:[0-5][0-9]" + "|";
					max_minute_timeframe = std::to_string(time_max.hour) + ":" + std::to_string((time_max.minute/10)) + "[0-" + std::to_string(time_max.minute%10) + "]:[0-5][0-9]";
				}
				
				

			}
			else {
				if(( time_min.hour + 1 )!= time_max.hour) {
					hour_timeframe = "[" + std::to_string(time_min.hour+1) + "-" + std::to_string(time_max.hour-1) + "]" + ":[0-5][0-9]:[0-5][0-9]" + "|";
				}
				if(time_min.minute < 50) {				
					min_minute_timeframe = std::to_string(time_min.hour) + ":[" + std::to_string((time_min.minute/10+1)) + "-5][0-9]:[0-5][0-9]" + "|" 
							       + std::to_string(time_min.hour) + ":" + std::to_string(time_min.minute/10) + "[" + std::to_string(time_min.minute%10) + "-9]:[0-5][0-9]" + "|";
				}
				else {
					min_minute_timeframe = std::to_string(time_min.hour) + ":5[" + std::to_string(time_min.minute % 10) + "-9]:[0-5][0-9]"+ "|";
				}	
				if(time_max.minute > 10) {
					max_minute_timeframe = std::to_string(time_max.hour) + ":[0-" + std::to_string((time_max.minute/10-1)) + "][0-9]:[0-5][0-9]" + "|"
							       + std::to_string(time_max.hour) + ":" + std::to_string(time_max.minute/10) + "[0-" + std::to_string(time_max.minute%10) + "]:[0-5][0-9]";
				}
				else {
					max_minute_timeframe = std::to_string(time_max.hour) + ":0[0-" + std::to_string((time_max.minute%10)) + "]:[0-5][0-9]";
				}
			}
			timeframe = hour_timeframe + min_minute_timeframe + max_minute_timeframe;		
		}
		else if ( strcmp(arg,"-h") == 0 || strcmp(arg,"--help") == 0) {
			this->PrintUsage();
			exit(EXIT_FAILURE);		
		}
		else {
			if ( setting_inst.inputfile == true ) {
				std::cerr << "ERROR: More than one input file provided" << std::endl;
				this->PrintUsage();
				exit (EXIT_FAILURE);
			}
			inputfile = arg;
			ifs.open(arg);
			if(!ifs.is_open()) {
				std::cerr << "ERROR: Unable to open " << arg << " for reading" << std::endl;
				this->PrintUsage();
				exit (EXIT_FAILURE);
			}
			ifs.close();
			setting_inst.inputfile = true;
		}
		index++;	
	}
	if(setting_inst.inputfile == false) {
		std::cerr << "ERROR: No inputfile provided" << std::endl;
		this->PrintUsage();
		exit (EXIT_FAILURE);
	}
	if(setting_inst.timeframe == false) {
		std::cerr << "ERROR: No timeframe provided" << std::endl;
		this->PrintUsage();
		exit (EXIT_FAILURE);	
	}
}


/*
 *	ReadNominees Function 
 *	No inputs and no outputs
 *	The function reads in the nominees from a given file by the name of nominees.txt
 */
void SocialData::ReadNominees() {
	std::string buf;
	std::ifstream ifs("nominees.txt");
	if(!ifs.is_open()) {
		std::cerr << "ERROR: Cannot open files containing nominee information nominees.txt" << std::endl;
		exit (EXIT_FAILURE);
	}
	while(!ifs.fail()) {
		getline(ifs,buf);
		if(ifs.fail()) {
			break;		
		}
		if(number_of_nominees == 0) {
			this->winner = buf;
		}
		nominee_node n;
		n.nominee = buf;
		nominees.push_back(n);
		number_of_nominees++;
	}
	ifs.close();
}


/*
 *	ParseFile Function 
 *	No inputs and no outputs
 *	The function generates a subset of data according to the specific time frame and also the nominees
 */
void SocialData::ParseFile() {
	std::string command = "egrep --ignore-case -r \"("+timeframe+").*(";
	std::string pipeline_command = " " + inputfile + " | cut -d' ' -f1,2,3,5 --complement | cut -d',' -f2 --complement > _framedata.txt";
	for(int i = 0; i < number_of_nominees; i++) {
		buf_nominees = buf_nominees + nominees[i].nominee;
		if(i != number_of_nominees-1) {
			buf_nominees = buf_nominees + "|";		
		}
		else {
			buf_nominees = buf_nominees + ")\"";		
		}
	}
	invoke = command + buf_nominees + pipeline_command;	
	system(invoke.c_str());
}


/*
 *	RunSubSet Function 
 *	No inputs and no outputs
 *	The function runs grep and finds the number of times a user mentions each nominee
 */
void SocialData::RunSubSet() {
	std::ifstream ifs("_framedata.txt");
	if(!ifs.is_open()) {
		std::cerr << "ERRROR: Cannot open stored data file." << std::endl;
		exit( EXIT_FAILURE );
	}

	int count = 1;
	std::string cmd = "egrep --ignore-case ";
	std::string cmd_end = " ./_framedata.txt | wc -l";	
	for(int i = 0; i < number_of_nominees; i++) {	
		if(nominees[i].nominee == winner) {
			this->PredictionTimeFrame();
			invoke = cmd + "\"" + nominees[i].nominee + "\"" + cmd_end;
			buffer b2(invoke.c_str());
			std::istream command_count(&b2);
			command_count >> count;
			nominees[i].popularity = count;
		}
		else {
			invoke = cmd + "\"" + nominees[i].nominee + "\"" + cmd_end;
			buffer b(invoke.c_str());
			std::istream command(&b);
		
			command >> count;
			nominees[i].popularity = count;
		}
	}
	ifs.close();
}


/*
 *	PredictionTimeFrame Function 
 *	No inputs and no outputs
 *	The function finds the minute and hour when the winner is mentioned the most within the dataset
 */
void SocialData::PredictionTimeFrame() {
	time_s prev;
	int count = 1;
	std::string hr_buf, min_buf, sec_buf;
	std::string cmd = "egrep --ignore-case ";
	invoke = cmd + "\"" + winner + "\"" + " ./_framedata.txt";
	buffer b(invoke.c_str());
	std::istream command(&b);
	while(!command.fail()) {
		std::getline(command, hr_buf, ':');
		std::getline(command, min_buf, ':');
		std::getline(command, sec_buf, ' ');
		if(command.fail()) {
			break;
		}
		if( prev.minute == std::atoi(min_buf.c_str()) && prev.hour == std::atoi(hr_buf.c_str())) {
			count++;
		}
		else {
			count = 1;
		}
		if(prediction.count < count) {
			prediction.hour = std::atoi(hr_buf.c_str());
			prediction.minute = std::atoi(min_buf.c_str());
			prediction.count = count;
		}
		std::getline(command, trash, '\n');
		prev.minute = atoi(min_buf.c_str());
		prev.hour = std::atoi(hr_buf.c_str());
	}
	//std::cout << prediction.hour << ":" << prediction.minute << std::endl;
}


/*
 *	LocationActivity Function 
 *	No inputs and no outputs
 *	Function processes information about which states were active during the oscars
 */
void SocialData::LocationActivity() {
	invoke = "egrep \"("+timeframe+").*(#Oscar2015)\" " +inputfile + " | cut -d',' -f3- > ./_hashtagdata.txt";
	system(invoke.c_str());
	std::string state_buf, ab_buf;
	std::ifstream ifs("states.txt");
	state_node newStateNode;
	if(!ifs.is_open()) {
		std::cerr << "ERROR: Cannot open file containing state information states.txt" << std::endl;
	}
	while(!ifs.fail()) {
		std::getline(ifs,state_buf,',');
		std::getline(ifs,ab_buf);
		if(ifs.fail()) {
			break;
		}
		invoke = "egrep \"" + state_buf + "| " + ab_buf + " |" + ab_buf + "\\\"" + "|" + ab_buf +"," "\" ./_hashtagdata.txt | wc -l";
		buffer b(invoke.c_str());
		newStateNode.state = state_buf;
		newStateNode.abbreviation = ab_buf;
		std::istream command(&b);
		command >> newStateNode.occurance;
		if(newStateNode.occurance != 0) {
			states.push_back(newStateNode);
		}
	}
	std::sort(states.begin(), states.end(), comparitor_states());
	ifs.close();
}


/*
 *	Simulate Function 
 *	No inputs and no outputs
 *	Main function to process data and also compute results
 */
void SocialData::Simulate() {
	ReadNominees();
	ParseFile();
	RunSubSet();
	LocationActivity();
	if(setting_inst.outputfile == false) {
		PrintResult(std::cout);
	}
	else {
		PrintResult(ofs);
		ofs.close();
	}
}


/*
 *	PrintResult Function 
 *	1 input of type ostream and no outputs
 *	The function prints out the results of parsing the input file as requested
 */
void SocialData::PrintResult(std::ostream &out) {
	std::sort(nominees.begin(), nominees.end(), comparitor());
	out << "Popularity Rank:" << std::endl;
	for(int i = 0;i < number_of_nominees; i++) {
		out << std::setw(5) << std::left << i+1 << nominees[i].nominee << std::endl;
	}
	out << "Winner Announcment Prediction:" << std::endl;
	out << "The winner " << winner << " was mentioned most at " << std::setw(2) << std::setfill('0') << std::right << prediction.hour << ":" << std::setw(2) << std::setfill('0') << prediction.minute << " UTC"<< std::endl;
	out << "Location:" << std::endl;
	for(size_t i= 0; i < states.size(); i++) {
		out << std::setw(5) << std::setfill(' ') << std::left << i+1 << states[i].state << std::endl;
	}
}


/*
 *	PrintUsage Function 
 *	No inputs and no outputs
 *	The function prints proper usage of SocialData program
 */
void SocialData::PrintUsage() {
	std::cout << "Social Data Program by Madhav Patel" << std::endl << std::endl
		  << "Usage: SocialData [options] inputfile" << std::endl
		  << "Options:" << std::endl
		  << std::setw(20) << std::left << "  -o" << "this option is utilized to specify an outputfile if none are provided then it displays results to standard output" << std::endl
		  << std::setw(20) << std::left << "  -t" << "specify the time frame from which to gather information -t XX:XX-XX:XX (Time should be provided in UTC)." << std::endl
		  << std::setw(20) << std::left << "  -h --help" << "display the help option" << std::endl;
	
}
