#include <iostream>
#include <vector>
#include <fstream>
#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <iomanip>
#include <string.h>

class SocialData
{
public:
	SocialData();
	~SocialData();
	void ReadNominees();
	void ParseFile();
	void ParseCMDArgs(int argc, char* argv[]);
	void RunSubSet();
	void PredictionTimeFrame();
	void LocationActivity();
	void PrintResult(std:: ostream & out);
	void PrintUsage();
	void Simulate();
private:
	struct buffer: public std::streambuf
	{   
	    typedef std::streambuf::traits_type traits;
	    typedef traits::int_type            int_type;

	    buffer(std::string const& command)
		: stream(popen(command.c_str(), "r"))
	    {   
		this->setg(&c[0], &c[0], &c[0]);
		this->setp(0, 0); 
	    }   
	    ~buffer()
	    {   
		if (stream != NULL)
		{   
		    fclose(stream);
		}   
	    }   
	    virtual int_type underflow()
	    {   
		std::size_t size = fread(c, 1, 100, stream);
		this->setg(&c[0], &c[0], &c[size]);

		return size == 0 ? EOF : *c; 
	    }   

	    private:
		FILE*   stream;
		char    c[100];

	};  

	struct nominee_node {
		std::string nominee;
		int popularity;
		};

	struct comparitor {
		bool operator() ( nominee_node const &lhs, nominee_node const &rhs ) const { 
		return lhs.popularity > rhs.popularity;
	 	}
	};
	
	struct state_node {
		std::string state;
		std::string abbreviation;
		int occurance;
	};

	struct comparitor_states {
		bool operator() ( state_node const &lhs, state_node const &rhs ) const {
		return lhs.occurance > rhs.occurance;
		}
	};	
	struct time_s {
		int hour;
		int minute;
		int count;
	}time_min, time_max, prediction;
	
	struct setting {
		bool timeframe;
		bool outputfile;
		bool inputfile;		
	}setting_inst;

	std::vector<nominee_node> nominees;
	std::vector<state_node> states;
	std::string buf_nominees, trash, invoke, winner, buf, inputfile, timeframe;
	std::ofstream ofs;
	int number_of_nominees;
};

