#include <iostream>
#include <string>
#include "SocialData.h"



int main(int argc, char* argv[]) {
	SocialData instance;
	instance.ParseCMDArgs(argc, argv);
	instance.Simulate();

	return 0;			
}


