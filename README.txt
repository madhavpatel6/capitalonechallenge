It is very important that the inputfile also be extracted to this folder with the name of oscar_tweets.csv

To invoke the program with the required specifications for the challenege utilize the script file I have made. Just simply execute ./run or ./SocialData ./oscar_tweets.csv -t 1:30-4:30

Example Output:

Popularity Rank:
1    Selma
2    Whiplash
3    Boyhood
4    Birdman
5    The Grand Budapest Hotel
6    American Sniper
7    The Imitation Game
8    The Theory of Everything
Winner Announcment Prediction:
The winner Birdman was mentioned most at 03:26 UTC
Location:
1    California
2    New York
3    Texas
4    Florida
5    Arizona
6    Washington
7    Maryland
8    Pennsylvania
9    Illinois
10   North Carolina
11   New Hampshire
12   Oregon
13   Delaware
14   Connecticut
15   Indiana
16   Kentucky
17   Tennessee
18   Louisiana
19   Colorado
20   Michigan
21   Nebraska
